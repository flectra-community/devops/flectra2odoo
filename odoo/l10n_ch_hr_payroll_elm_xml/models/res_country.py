import logging
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class CountryState(models.Model):
    # Private attributes
    _inherit = 'res.country.state'

    # Default methods

    # Fields declaration
    expense_regulation = fields.Boolean(
        string='Expense Regulation',
    )

    date_approval = fields.Date(
        string='Date of Approval',
    )

    # Compute and search methods, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
