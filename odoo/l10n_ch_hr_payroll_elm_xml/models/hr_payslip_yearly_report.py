import logging
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class HrPayslipYearlyReport(models.Model):
    _inherit = 'hr.payslip.yearly.report'

    salary_statement_group = fields.Selection(
            string='Salary Statement Group',
            selection=[
                ('income', 'Income'),
                ('gross', 'Gross Income'),
                ('net', 'Net Income'),
                ('fringe_bene_car', 'Fringe Benefit Car'),
                ('other_bene', 'Other Benefits'),
                ('oai', 'OAI'),
                ('obp', 'OBP'),
                ('education', 'Education'),
                ('lump_sum_rep', 'Lump Sum Representation'),
                ('lump_sum_car', 'Lump Sum Car'),
                ('lump_sum_other', 'Lump Sum Others'),
                ('effective', 'Effective Others'),
                ('fam_allowance', 'Family Allowance'),
                ('source_deduction', 'Deduction at Source'),
                ('remark', 'Remark'),
            ],
    )

    @api.model
    def _select(self):
        select_fields = super()._select()
        select_fields.append("hsr.salary_statement_group AS salary_statement_group")
        return select_fields
