from . import res_country
from . import res_company
from . import hr_salary_rule
from . import hr_payroll_config
from . import hr_payslip_yearly_report