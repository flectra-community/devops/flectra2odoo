{
    'name': "Payroll ELM XML Export",
    'summary': """Export an importable xml file with payroll data""",
    'author': 'Jamotion GmbH, 2BIT GmbH, Jamotion GmbH',
    'website': 'https://gitlab.com/jamotion/odoo',
    'category': 'Human Resources',
    'version': '14.0.1.0.0',
    # any module necessary for this one to work correctly
    'depends': [
        'l10n_ch_hr_payroll_base',
    ],
    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/hr_payroll_config_view.xml',
        'views/hr_salary_rule_views.xml',
        'views/res_country_view.xml',
        'wizards/hr_payslip_elm_xml_export_wizard.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'test': [],
    'images': [],
    'qweb': [],
    'application': True,
    'installable': True,
    'auto_install': False,
    'price': 15,
    'license': 'AGPL-3',
    'currency': 'USD',
}
