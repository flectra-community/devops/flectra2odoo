import base64
import logging
from datetime import datetime

from odoo import models, fields, api, _

from lxml import etree as et

from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)

NAMESPACE_URL = 'http://www.swissdec.ch/schema/sd/20091204/SalaryDeclaration'


def _s(name):
    return '{%s}%s' % (NAMESPACE_URL, name)


class HrPayslipElmXmlExport(models.TransientModel):
    # Private attributes
    _name = 'hr.payslip.elm.xml.export.wizard'
    _description = 'Wizard to configure payslip xml export'

    # Default methods

    # Fields declaration
    year = fields.Char(
        string='Year',
        default=lambda s: str(fields.date.today().year - 1),
        required=True,
    )

    name = fields.Char(
        string='Name',
    )

    data = fields.Binary(
        string='Data',
        readonly=True,
    )

    wizard_date_from = fields.Date(
        string='Wizard Date From',
    )

    wizard_date_to = fields.Date(
        string='Wizard Date To',
    )

    # Compute and search methods, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods
    def export_xml(self):
        self.ensure_one()
        xml_data = self.build_payslip_xml()
        return self.download_xml(xml_data)

    # Business methods
    def build_payslip_xml(self):
        company = self.create_uid.company_id

        self.wizard_date_from = self.year + '-01-01'
        self.wizard_date_to = self.year + '-12-31'
        search_domain = [
            ('date_from', '>=', self.wizard_date_from),
            ('date_from', '<=', self.wizard_date_to),
        ]
        contracts = self.env['hr.payslip'].search(search_domain).mapped('contract_id')

        et.register_namespace('sd', NAMESPACE_URL)
        root = et.Element(_s('SalaryDeclaration'))
        root.attrib['schemaVersion'] = '0.0'
        Company = et.SubElement(root, _s('Company'))
        CompanyDescription = et.SubElement(Company, _s('CompanyDescription'))
        AddressPosition = et.SubElement(CompanyDescription, _s('AddressPosition'))
        if not company.address_alignment:
            raise ValidationError(
                _('Please specify the address alignment for company {name} in the '
                  'payroll settings.').format(name=company.name)
            )

        AddressPosition.text = company.address_alignment
        Name = et.SubElement(CompanyDescription, _s('Name'))
        HR_RC_Name = et.SubElement(Name, _s('HR-RC-Name'))
        HR_RC_Name.text = company.name
        Address = et.SubElement(CompanyDescription, _s('Address'))
        Street = et.SubElement(Address, _s('Street'))
        Street.text = company.street
        # Postbox = et.SubElement(Address, _s('Postbox')) --> So far not used in odoo
        ZIP_Code = et.SubElement(Address, _s('ZIP-Code'))
        ZIP_Code.text = company.zip
        City = et.SubElement(Address, _s('City'))
        City.text = company.city
        Country = et.SubElement(Address, _s('Country'))
        Country.text = company.country_id.name
        BUR_REE = et.SubElement(CompanyDescription, _s('BUR-REE'))
        CompanyWorkingTime = et.SubElement(BUR_REE, _s('CompanyWorkingTime'))
        CompanyWorkingTime.attrib['CompanyWorkingTimeID'] = '#1'
        WeeklyHours = et.SubElement(CompanyWorkingTime, _s('WeeklyHours'))
        Staff = et.SubElement(Company, _s('Staff'))

        for employee in contracts.mapped('employee_id'):
            emp_contracts = contracts.filtered(lambda f: f.employee_id == employee)
            if not emp_contracts:
                raise ValidationError(_('No contracts found for employee {emp} in the selected period.').format(
                    emp=employee.name,
                ))
            has_multi = len(emp_contracts) > 1
            Staff.append(self._build_employee_data(employee, emp_contracts, has_multi))

        GeneralSalaryDeclarationDescription = et.SubElement(root, _s('GeneralSalaryDeclarationDescription'))
        CreationDate = et.SubElement(GeneralSalaryDeclarationDescription, _s('CreationDate'))
        CreationDate.text = datetime.now().isoformat()[:23] + 'Z'
        AccountingPeriod = et.SubElement(GeneralSalaryDeclarationDescription, _s('AccountingPeriod'))
        AccountingPeriod.text = self.year
        ContactPerson = et.SubElement(GeneralSalaryDeclarationDescription, _s('ContactPerson'))
        Name = et.SubElement(ContactPerson, _s('Name'))
        Name.text = self.env.user.partner_id.name
        PhoneNumber = et.SubElement(ContactPerson, _s('PhoneNumber'))
        if not self.env.user.partner_id.phone:
            raise ValidationError(_('A phone number of the current user is required.'))

        PhoneNumber.text = self.env.user.partner_id.phone

        xml_data = et.tostring(root)

        return xml_data

    def _build_employee_data(self, employee, contracts, has_multi):
        if not employee.address_home_id:
            raise ValidationError(
                _('You have to specify a home address for employee {name}.\n '
                  'Create a partner for this employee to do so.').format(name=employee.name)
            )

        Person = et.Element(_s('Person'))
        Particulars = et.SubElement(Person, _s('Particulars'))
        Language = et.SubElement(Particulars, _s('Language'))
        employee_lang = self._get_employee_lang(employee)
        Language.text = employee_lang
        DateOfBirth = et.SubElement(Particulars, _s('DateOfBirth'))
        DateOfBirth.text = employee.birthday
        if not employee.identification_id:
            raise ValidationError(
                _('No social insurance identification registered for employee {name}.').format(name=employee.name)
            )

        Social_InsuranceIdentification = et.SubElement(Particulars, _s('Social-InsuranceIdentification'))
        SV_AS_Number = et.SubElement(Social_InsuranceIdentification, _s('SV-AS-Number'))
        SV_AS_Number.text = employee.identification_id
        Lastname = et.SubElement(Particulars, _s('Lastname'))
        Lastname.text = getattr(employee, 'lastname', employee.name.rsplit(' ', 1)[-1])
        Firstname = et.SubElement(Particulars, _s('Firstname'))
        Firstname.text = getattr(employee, 'firstname', employee.name.split(' ')[0])

        Address = et.SubElement(Particulars, _s('Address'))
        if employee.address_home_id.street:
            Street = et.SubElement(Address, _s('Street'))
            Street.text = employee.address_home_id.street
        # Postbox = et.SubElement(Address, _s('Postbox')) --> So far not used in odoo
        if not employee.address_home_id.zip or not employee.address_home_id.city:
            raise ValidationError(
                _('Home address of employee {name} needs a valid ZIP and city.').format(name=employee.name)
            )

        ZIP_Code = et.SubElement(Address, _s('ZIP-Code'))
        ZIP_Code.text = employee.address_home_id.zip
        City = et.SubElement(Address, _s('City'))
        City.text = employee.address_home_id.city
        if employee.address_home_id.country_id:
            Country = et.SubElement(Address, _s('Country'))
            Country.text = employee.address_home_id.country_id.name

        Work = et.SubElement(Person, _s('Work'))
        TaxSalaries = et.SubElement(Person, _s('TaxSalaries'))
        TaxSalaries.append(self._build_summed_payslips(employee, contracts))

        return Person

    def _build_summed_payslips(self, employee, contracts):
        active_contract = contracts[-1]

        income_codes = self._get_codes_for_salary_rule('income')
        gross_income_codes = self._get_codes_for_salary_rule('gross')
        net_income_codes = self._get_codes_for_salary_rule('net')
        fringe_benefit_car_codes = self._get_codes_for_salary_rule('fringe_bene_car')
        other_benefit_codes = self._get_codes_for_salary_rule('other_bene')
        oai_codes = self._get_codes_for_salary_rule('oai')
        obp_codes = self._get_codes_for_salary_rule('obp')
        education_codes = self._get_codes_for_salary_rule('education')
        charges_lump_sum_rep_codes = self._get_codes_for_salary_rule('lump_sum_rep')
        charges_lump_sum_car_codes = self._get_codes_for_salary_rule('lump_sum_car')
        charges_lump_sum_other_codes = self._get_codes_for_salary_rule('lump_sum_other')
        charges_effective_other_codes = self._get_codes_for_salary_rule('effective')
        family_allowances_codes = self._get_codes_for_salary_rule('fam_allowance')
        source_deduction_codes = self._get_codes_for_salary_rule('source_deduction')
        remark_codes = self._get_codes_for_salary_rule('remark')

        # We use already available data from hr.payslip.yearly.report instead of recalculating
        data = self.env['hr.payslip.yearly.report'].search([
            ('employee_id', '=', employee.id),
            ('year', '=', self.year),
        ])

        income_amount = sum(data.filtered(lambda f: f.code in income_codes).mapped('total'))
        gross_income = sum(data.filtered(lambda f: f.code in gross_income_codes).mapped('total'))
        net_income = sum(data.filtered(lambda f: f.code in net_income_codes).mapped('total'))
        fringe_benefit_car_amount = sum(data.filtered(lambda f: f.code in fringe_benefit_car_codes).mapped('total'))
        other_benefit_amount = sum(data.filtered(lambda f: f.code in other_benefit_codes).mapped('total'))
        other_benefit_text = ''
        if other_benefit_amount != 0:
            other_benefit_text = data.filtered(lambda f: f.code in other_benefit_codes).mapped('name')[0]
        obp_amount = sum(data.filtered(lambda f: f.code in obp_codes).mapped('total'))
        oai_amount = sum(data.filtered(lambda f: f.code in oai_codes).mapped('total'))
        lump_sum_rep_amount = sum(data.filtered(lambda f: f.code in charges_lump_sum_rep_codes).mapped('total'))
        lump_sum_car_amount = sum(data.filtered(lambda f: f.code in charges_lump_sum_car_codes).mapped('total'))
        lump_sum_other_amount = sum(data.filtered(lambda f: f.code in charges_lump_sum_other_codes).mapped('total'))
        education_contribution_amount = sum(data.filtered(lambda f: f.code in education_codes).mapped('total'))
        effective_other_amount = sum(data.filtered(lambda f: f.code in charges_effective_other_codes).mapped('total'))
        family_allowances_amount = sum(data.filtered(lambda f: f.code in family_allowances_codes).mapped('total'))
        source_deduction_amount = sum(data.filtered(lambda f: f.code in source_deduction_codes).mapped('total'))

        # Here, we prepare the text for remark field
        remark_data = {}
        for line in data.filtered(lambda f: f.code in remark_codes):
            if line.code not in remark_data:
                remark_data[line.code] = {'name': line.name, 'total': 0.0}
            remark_data[line.code]['total'] += line.total

        remark_text_parts = []
        for code, remark_entry in remark_data.items():
            remark_text_parts.append('{remark}: {amount}'.format(
                remark=remark_entry['name'],
                amount=abs(int(remark_entry['total'])),
            ))
        if active_contract.payroll_remark:
            remark_text_parts.append(active_contract.payroll_remark)
        remark_text = '; '.join(remark_text_parts)

        period_date_from = self.wizard_date_from
        period_date_to = self.wizard_date_to

        if min(contracts.mapped('date_start')) > self.wizard_date_from:
            period_date_from = min(contracts.mapped('date_start'))

        if not contracts.filtered(lambda f: not f.date_end):
            max_contract_date_end = max(contracts.mapped('date_end'))
            if max_contract_date_end < period_date_to:
                period_date_to = max_contract_date_end

        TaxSalary = et.Element(_s('TaxSalary'))
        LohnausweisTyp = et.SubElement(TaxSalary, _s('LohnausweisTyp'))
        LohnausweisTyp.text = _('SALARY DECLARATION')
        Period = et.SubElement(TaxSalary, _s('Period'))
        Pfrom = et.SubElement(Period, _s('from'))
        Pfrom.text = period_date_from
        Puntil = et.SubElement(Period, _s('until'))
        Puntil.text = period_date_to
        Year = et.SubElement(TaxSalary, _s('Year'))
        Year.text = self.year
        Income = et.SubElement(TaxSalary, _s('Income'))
        Income.text = str(int(income_amount))
        FringeBenefits = et.SubElement(TaxSalary, _s('FringeBenefits'))
        if int(abs(fringe_benefit_car_amount)) > 0:
            CompanyCar = et.SubElement(FringeBenefits, _s('CompanyCar'))
            company_car_value = int(abs(fringe_benefit_car_amount))
            CompanyCar.text = str(company_car_value)
            if company_car_value:
                TaxSalary.insert(1, et.Element(_s('FreeTransport')))
        FoodLodging = et.SubElement(FringeBenefits, _s('FoodLodging'))
        if family_allowances_amount:
            Other = et.SubElement(FringeBenefits, _s('Other'))
            Sum = et.SubElement(Other, _s('Sum'))
            Sum.text = str(int(family_allowances_amount))
            Text = et.SubElement(Other, _s('Text'))
            Text.text = _('Family Allowances')
        SporadicBenefits = et.SubElement(TaxSalary, _s('SporadicBenefits'))
        Sum = et.SubElement(SporadicBenefits, _s('Sum'))
        Text = et.SubElement(SporadicBenefits, _s('Text'))
        CapitalPayment = et.SubElement(TaxSalary, _s('CapitalPayment'))
        Sum = et.SubElement(CapitalPayment, _s('Sum'))
        Text = et.SubElement(CapitalPayment, _s('Text'))
        OwnershipRight = et.SubElement(TaxSalary, _s('OwnershipRight'))
        BoardOfDirectorsRemuneration = et.SubElement(TaxSalary, _s('BoardOfDirectorsRemuneration'))
        OtherBenefits = et.SubElement(TaxSalary, _s('OtherBenefits'))
        Sum = et.SubElement(OtherBenefits, _s('Sum'))
        Sum.text = str(int(other_benefit_amount))
        Text = et.SubElement(OtherBenefits, _s('Text'))
        Text.text = other_benefit_text
        GrossIncome = et.SubElement(TaxSalary, _s('GrossIncome'))
        GrossIncome.text = str(int(gross_income))
        AHV_ALV_NBUV_AVS_AC_AANP_Contribution = et.SubElement(TaxSalary, _s('AHV-ALV-NBUV-AVS-AC-AANP-Contribution'))
        AHV_ALV_NBUV_AVS_AC_AANP_Contribution.text = str(int(abs(oai_amount)))
        BVG_LPP_Contribution = et.SubElement(TaxSalary, _s('BVG-LPP-Contribution'))
        # Purchase = et.SubElement(BVG_LPP_Contribution, _s('Purchase')) --> So far not handled in odoo
        Regular = et.SubElement(BVG_LPP_Contribution, _s('Regular'))
        Regular.text = str(int(abs(obp_amount)))
        NetIncome = et.SubElement(TaxSalary, _s('NetIncome'))
        NetIncome.text = str(int(net_income))
        DeductionAtSource = et.SubElement(TaxSalary, _s('DeductionAtSource'))
        DeductionAtSource.text = str(int(abs(source_deduction_amount)))
        Charges = et.SubElement(TaxSalary, _s('Charges'))
        LumpSum = et.SubElement(Charges, _s('LumpSum'))
        Car = et.SubElement(LumpSum, _s('Car'))
        Car.text = str(int(lump_sum_car_amount))
        Representation = et.SubElement(LumpSum, _s('Representation'))
        Representation.text = str(int(lump_sum_rep_amount))
        Other = et.SubElement(LumpSum, _s('Other'))
        Sum = et.SubElement(Other, _s('Sum'))
        Text = et.SubElement(Other, _s('Text'))
        if lump_sum_other_amount and lump_sum_other_amount > 0.0:
            Sum.text = str(int(lump_sum_other_amount))
            Text.text = active_contract.expense_lump_sum_other_text
        Education = et.SubElement(Charges, _s('Education'))
        Education.text = str(int(education_contribution_amount))
        Effective = et.SubElement(Charges, _s('Effective'))
        if active_contract.no_charge_declaration:
            et.SubElement(Effective, _s('TravelFoodAccommodation'))
        Other = et.SubElement(Effective, _s('Other'))
        Sum = et.SubElement(Other, _s('Sum'))
        Sum.text = str(int(effective_other_amount))
        Text = et.SubElement(Other, _s('Text'))
        OtherFringeBenefits = et.SubElement(TaxSalary, _s('OtherFringeBenefits'))
        Remark = et.SubElement(TaxSalary, _s('Remark'))
        Remark.text = str(remark_text)
        StandardRemark = et.SubElement(TaxSalary, _s('StandardRemark'))

        emp_work_addr_state = employee.company_id.partner_id.state_id
        if employee.address_id:
            emp_work_addr_state = employee.address_id.state_id

        if emp_work_addr_state.expense_regulation:
            SpesenReglementGenehmigt = et.SubElement(StandardRemark, _s('SpesenReglementGenehmigt'))
            Allowed = et.SubElement(SpesenReglementGenehmigt, _s('Allowed'))
            Allowed.text = emp_work_addr_state.date_approval
            Canton = et.SubElement(SpesenReglementGenehmigt, _s('Canton'))
            Canton.text = emp_work_addr_state.code
        geschaeftsFahrzeugAussendienstBerechnungsartValue = et.SubElement(
            TaxSalary, _s('geschaeftsFahrzeugAussendienstBerechnungsartValue')
        )
        Allowed = et.SubElement(geschaeftsFahrzeugAussendienstBerechnungsartValue, _s('Allowed'))
        Canton = et.SubElement(geschaeftsFahrzeugAussendienstBerechnungsartValue, _s('Canton'))

        return TaxSalary

    def _get_employee_lang(self, employee):
        emp_lang = 'de-CH'
        if employee.address_home_id and employee.address_home_id.lang in ['de_DE', 'de_CH', 'fr_FR', 'fr_CH', 'it_IT']:
            emp_lang = employee.address_home_id.lang.split('_')[0] + '-CH'
        return emp_lang

    def download_xml(self, file):
        self.write({'data': base64.encodestring(file), 'name': 'payroll_elm.xml'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.payslip.elm.xml.export.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'views': [(False, 'form')],
            'target': 'new',
        }

    def _get_codes_for_salary_rule(self, selection):
        codes = self.env['hr.salary.rule'].search([('salary_statement_group', '=', selection)]).mapped('code')
        return list(set(codes))
