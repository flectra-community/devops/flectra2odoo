from odoo.tests import common
from odoo.exceptions import ValidationError

from lxml import etree as ET


def _s(name):
    return '{http://www.swissdec.ch/schema/sd/20090803/SalaryDeclaration}' + name


class TestXml(common.TransactionCase):
    def setUp(self):
        super(TestXml, self).setUp()
        self.employee = self.env.ref('hr.employee_al')

    def test_build_payroll_xml(self):
        self.build_payroll_xml()

    def build_payroll_xml(self):
        ET.register_namespace('sd', 'http://www.swissdec.ch/schema/sd/20090803/SalaryDeclaration')
        root = ET.Element(_s('SalaryDeclaration'))
        root.attrib['schemaVersion'] = '0.0'
        Company = ET.SubElement(root, _s('Company'))
        CompanyDescription = ET.SubElement(Company, _s('CompanyDescription'))
        AddressPosition = ET.SubElement(CompanyDescription, _s('AddressPosition'))
        AddressPosition.text = 'RIGHT'
        Name = ET.SubElement(CompanyDescription, _s('Name'))
        HR_RC_Name = ET.SubElement(Name, _s('HR-RC-Name'))

        print('Here is your XML tree')
        print(ET.dump(root))

