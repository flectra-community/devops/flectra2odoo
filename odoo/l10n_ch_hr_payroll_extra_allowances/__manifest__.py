# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Switzerland - Payroll Extra Allowances',
    'summary': 'Switzerland Payroll Extra Allowances',
    'category': 'Localization',
    'author': "2BIT GmbH, Jamotion GmbH",
    'depends': [
        'l10n_ch_hr_payroll_base'
    ],
    'version': '14.0.1.0.0',
    'auto_install': False,
    'website': 'https://gitlab.com/jamotion/odoo',
    'license': 'AGPL-3',
    'price': 0.00,
    'currency': 'CHF',
    'data': [
        'security/ir.model.access.csv',
        
        'data/child_allowance_rules_data.xml',
        'data/hr.salary.rule.xml',

        'views/hr_employee_view.xml',
        'views/res_users.xml',
        'views/hr_payroll_config_view.xml',
        'views/hr_contract_views.xml'
    ],
    'demo': [
        
    ],
    'installable': True
}
