# Part of Odoo, Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, AccessError

class ChildAllowanceRule(models.Model):
    _name = "child.allowance.rule"

    region = fields.Char(string='Region', required=True)
    until_twelve_years_case = fields.Integer(string='Until 12 years')
    from_twelve_years_case = fields.Integer(string='From 12 years')
    in_further_education_case = fields.Integer(string='Is in further education?')
    birth_case = fields.Integer(string='Case of Birth')
    adoption_case = fields.Integer(string='Case of Adoption')

    associated_children = fields.One2many('hr.employee.children', 'child_allowance_rule_id', string='Associated Children')
