# Part of Odoo, Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, AccessError

class ChildAllowanceRule(models.Model):
    _name = "hr.payroll.config.ktg"

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', required=True)
    male_percentage = fields.Integer(string='Male Percentage')
    female_percentage = fields.Integer(string='Female Percentage')
      