# Part of Odoo, Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class Contract(models.Model):
    _inherit = 'hr.contract'

    ktg_solutions = fields.Many2one('hr.payroll.config.ktg', string='KTG Solutions')
