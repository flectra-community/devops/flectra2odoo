# Copyright 2017 Open Net S�rl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import odoo.addons.decimal_precision as dp
from odoo import models, fields, api

class HrPayrollConfig(models.TransientModel):
    _inherit = 'hr.payroll.config'

    ktg_solutions_id = fields.Many2many('hr.payroll.config.ktg', 'rel_hr_payroll_config_ktg', 'config_id', 'config_ktg_id', string='Associated KTG Solutions', store=True)
