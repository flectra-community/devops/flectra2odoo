# Part of Odoo, Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, AccessError

class HrEmployeePrivate(models.Model):
    _inherit = "hr.employee"

    employee_children = fields.One2many('hr.employee.children', 'hr_employee_id', string='Employee Children')
