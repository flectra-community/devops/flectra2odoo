# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging
from datetime import timedelta

import odoo.addons.decimal_precision as dp
from odoo import fields, models, api, _
from odoo.tools import float_compare, UserError, float_is_zero

_logger = logging.getLogger(__name__)

class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    working_days = fields.Integer(
        string='Number of working days')
    non_working_days = fields.Integer(
        string='Number of non-working days (not payable)')
    working_rate = fields.Float(
        string='Working Rate (%)',
        readonly=True,
        default=100)
    worked_hours = fields.Float(
        string='Number of worked hours',
        readonly=True,
        compute='_compute_worked_hours')

    wage_type = fields.Selection(related='contract_id.wage_type')

    def _compute_worked_hours(self):
        for payslip in self:
            sum_wo_sec = 0
            if payslip.contract_id.wage_type == 'hour':
                dt = fields.Datetime
                date_to = dt.from_string(payslip.date_to)
                date_to = dt.to_string(date_to + timedelta(days=1))

                all_time_records = self.env['hr.attendance'].search([
                    ('employee_id', '=', payslip.employee_id.id),
                    ('check_in', '>=', payslip.date_from),
                    ('check_in', '<', date_to)
                ])
                sum_all_hours = 0
                for time_rec in all_time_records:
                    sum_all_hours += time_rec.worked_hours

                sum_minutes = (sum_all_hours - int(sum_all_hours))*60
                sum_secunds = (sum_minutes - int(sum_minutes))/60
                sum_wo_sec = sum_all_hours - sum_secunds

            payslip.worked_hours = sum_wo_sec   

    @api.onchange('employee_id', 'date_from', 'date_to')
    def _onchange_employee_worked_hours(self):
        for payslip in self:
            payslip._compute_worked_hours()

    @api.onchange('working_days', 'non_working_days')
    def _onchange_working_non_working_days(self):
        for payslip in self:
            if payslip.working_days != 0:
                worked_days = payslip.working_days - payslip.non_working_days
                payslip.working_rate = \
                    worked_days/float(payslip.working_days)*100
            elif payslip.working_days == 0 and payslip.non_working_days == 0:
                payslip.working_rate = 100

    def compute_sheet(self):
        res = {}
        for payslip in self:
            payslip._onchange_working_non_working_days()
            payslip._onchange_employee_worked_hours()
            res[payslip.id] = super(HrPayslip, payslip).compute_sheet()
        return res

    def action_payslip_done_no_compute(self):
        result = self.write({'state': 'done'})
        precision = self.env['decimal.precision'].precision_get('Payroll')

        for slip in self:
            line_ids = []
            debit_sum = 0.0
            credit_sum = 0.0
            date = slip.date or slip.date_to

            name = _('Payslip of %s') % (slip.employee_id.name)
            move_dict = {
                'narration': name,
                'ref': slip.number,
                'journal_id': slip.journal_id.id,
                'date': date,
            }
            for line in slip.details_by_salary_rule_category:
                amount = slip.credit_note and -line.total or line.total
                if float_is_zero(amount, precision_digits=precision):
                    continue
                debit_account_id = line.salary_rule_id.account_debit.id
                credit_account_id = line.salary_rule_id.account_credit.id

                if debit_account_id:
                    debit_line = (0, 0, {
                        'name': line.name,
                        'partner_id': line._get_partner_id(credit_account=False),
                        'account_id': debit_account_id,
                        'journal_id': slip.journal_id.id,
                        'date': date,
                        'debit': amount > 0.0 and amount or 0.0,
                        'credit': amount < 0.0 and -amount or 0.0,
                        'analytic_account_id': line.salary_rule_id.analytic_account_id.id,
                        'tax_line_id': line.salary_rule_id.account_tax_id.id,
                    })
                    line_ids.append(debit_line)
                    debit_sum += debit_line[2]['debit'] - debit_line[2]['credit']

                if credit_account_id:
                    credit_line = (0, 0, {
                        'name': line.name,
                        'partner_id': line._get_partner_id(credit_account=True),
                        'account_id': credit_account_id,
                        'journal_id': slip.journal_id.id,
                        'date': date,
                        'debit': amount < 0.0 and -amount or 0.0,
                        'credit': amount > 0.0 and amount or 0.0,
                        'analytic_account_id': line.salary_rule_id.analytic_account_id.id,
                        'tax_line_id': line.salary_rule_id.account_tax_id.id,
                    })
                    line_ids.append(credit_line)
                    credit_sum += credit_line[2]['credit'] - credit_line[2]['debit']

            if float_compare(credit_sum, debit_sum, precision_digits=precision) == -1:
                acc_id = slip.journal_id.default_credit_account_id.id
                if not acc_id:
                    raise UserError(_('The Expense Journal "%s" has not properly configured the Credit Account!') % (slip.journal_id.name))
                adjust_credit = (0, 0, {
                    'name': _('Adjustment Entry'),
                    'partner_id': False,
                    'account_id': acc_id,
                    'journal_id': slip.journal_id.id,
                    'date': date,
                    'debit': 0.0,
                    'credit': debit_sum - credit_sum,
                })
                line_ids.append(adjust_credit)

            elif float_compare(debit_sum, credit_sum, precision_digits=precision) == -1:
                acc_id = slip.journal_id.default_debit_account_id.id
                if not acc_id:
                    raise UserError(_('The Expense Journal "%s" has not properly configured the Debit Account!') % (slip.journal_id.name))
                adjust_debit = (0, 0, {
                    'name': _('Adjustment Entry'),
                    'partner_id': False,
                    'account_id': acc_id,
                    'journal_id': slip.journal_id.id,
                    'date': date,
                    'debit': credit_sum - debit_sum,
                    'credit': 0.0,
                })
                line_ids.append(adjust_debit)
            move_dict['line_ids'] = line_ids
            move = self.env['account.move'].create(move_dict)
            slip.write({'move_id': move.id, 'date': date})
            move.post()
        return result

class HrPayslipLine(models.Model):

    _inherit = 'hr.payslip.line'

    python_rate = fields.Float(
        compute='_compute_python_rate',
        string='Rate (%)',
        digits=dp.get_precision('Payroll Rate'),
        store=True)
    python_amount = fields.Float(
        compute='_compute_python_amount',
        string='Amount',
        digits=dp.get_precision('Account'),
        store=True)

    manual_rate = fields.Float(
            string='Manual Rate (%)',
            digits=dp.get_precision('Payroll Rate'),
    )
    manual_amount = fields.Float(
            string='Manual Amount',
            digits=dp.get_precision('Account'),
    )

    @api.depends('quantity', 'amount', 'rate', 'manual_amount', 'manual_rate')
    def _compute_python_rate(self):
        for line in self:
            if line.manual_rate:
                line.python_rate = line.manual_rate
            elif line.salary_rule_id.percentage:
                line.python_rate = line.salary_rule_id.percentage
            elif line.rate:
                line.python_rate = line.rate
            else:
                line.python_rate = 100

    @api.depends('quantity', 'amount', 'rate', 'manual_amount', 'manual_rate')
    def _compute_python_amount(self):
        for line in self:
            if line.manual_amount:
                line.python_amount = line.manual_amount
            elif line.salary_rule_id.amount_base:
                line.python_amount = line.salary_rule_id.amount_base
            elif line.amount:
                line.python_amount = line.amount
            else:
                line.python_amount = 0

    @api.depends('quantity', 'amount', 'rate', 'manual_amount', 'manual_rate')
    def _compute_total(self):
        for line in self:
            line.total = float(line.quantity) * line.python_amount * line.python_rate / 100  