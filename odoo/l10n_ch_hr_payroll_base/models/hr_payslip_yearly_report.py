# Copyright 2017 Julien Coux (Camptocamp)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import odoo.addons.decimal_precision as dp

from odoo import tools
from odoo import models, fields, api


class HrPayslipYearlyReport(models.Model):
    _name = 'hr.payslip.yearly.report'
    _auto = False

    employee_id = fields.Many2one(
        comodel_name='hr.employee',
        string='Employee'
    )
    code = fields.Char(
        string='Code',
    )
    name = fields.Char(
        string='Name',
    )
    code_and_name = fields.Char(
        string='Code and name',
    )
    year = fields.Char(
        string='Year',
    )
    month = fields.Selection(
        selection=[
            ('01', 'January'), ('02', 'February'), ('03', 'March'),
            ('04', 'April'), ('05', 'May'), ('06', 'June'),
            ('07', 'July'), ('08', 'August'), ('09', 'September'),
            ('10', 'October'), ('11', 'November'), ('12', 'December')
        ],
    )
    total = fields.Monetary(
        string='Total',
        digits=dp.get_precision('Payroll')
    )
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        string='Currency'
    )

    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ])
    account_debit = fields.Many2one(
            comodel_name='account.account',
            string='Debit',
    )
    account_credit = fields.Many2one(
            comodel_name='account.account',
            string='Credit',
    )

    @api.model
    def _select(self):
        return [
            "hpl.id AS id",
            "hp.employee_id AS employee_id",
            "em.gender as gender",
            "hpl.code AS code",
            "hpl.name AS name",
            "hpl.code || ' - ' || hpl.name AS code_and_name",
            """CASE hp.credit_note 
               WHEN FALSE 
               THEN hsr.account_debit
               ELSE hsr.account_credit
               END AS account_debit""",
            """CASE hp.credit_note 
               WHEN FALSE 
               THEN hsr.account_credit
               ELSE hsr.account_debit
               END AS account_credit""",
            "TO_CHAR(hp.date_from, 'YYYY') as year",
            "TO_CHAR(hp.date_from, 'MM') as month",
            """CASE hp.credit_note 
               WHEN FALSE 
               THEN hpl.total
               ELSE hpl.total *-1
               END AS total""",
            "c.currency_id AS currency_id",
        ]

    @api.model
    def _from(self):
        return [
            "hr_payslip_line hpl",
            "INNER JOIN hr_salary_rule hsr ON hsr.id = hpl.salary_rule_id",
            "INNER JOIN hr_payslip hp ON hpl.slip_id = hp.id",
            "INNER JOIN hr_employee em ON em.id = hp.employee_id",
            "INNER JOIN res_company c ON hp.company_id = c.id",
        ]

    def init(self):
        tools.drop_view_if_exists(self._cr, 'hr_payslip_yearly_report')

        select_fields = self._select()
        field_str = ','.join(select_fields)

        tables = self._from()
        table_str = ' '.join(tables)

        query = """CREATE OR REPLACE VIEW hr_payslip_yearly_report AS (
                    SELECT %s
                    FROM %s
            )""" % (field_str, table_str)
        self._cr.execute(query)
