# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from . import hr_payslip
from . import hr_salary_rule
from . import hr_salary_rule_category
from . import res_company
from . import hr_payroll_config
from . import hr_contract
from . import hr_payslip_run
from . import hr_payroll_structure
from . import hr_payslip_yearly_report
from . import hr_employee