import logging
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class HrPayrollStructure(models.Model):
    _inherit = 'hr.payroll.structure'

    all_rule_ids = fields.One2many(
        comodel_name='hr.salary.rule',
        string='All Rules (recursive)',
        compute='_compute_all_rule_ids',
    )

    def _compute_all_rule_ids(self):
        for record in self:
            rules = record._get_all_rules()
            record.all_rule_ids = rules

    def _get_all_rules(self):
        self.ensure_one()
        rules = self.rule_ids | self.rule_ids.mapped('child_ids')
        if self.parent_id:
            rules = rules | self.parent_id._get_all_rules()
        rules = rules.sorted(key=lambda s: s.sequence)
        return rules
