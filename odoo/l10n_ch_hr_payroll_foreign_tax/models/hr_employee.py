
from odoo import models, api, fields, _
from odoo.exceptions import UserError, ValidationError, MissingError

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.depends('address_home_id.state_id')
    def _compute_foreign_tax_id(self):
        for emp in self:
            emp.foreign_tax_id = False
            foreign_taxes = self.env['payroll.foreign.tax'].search([('row_type', '=', '06'), ('canton', '=', emp.address_home_id.state_id.code), ('qst_code', '=', emp.contract_id.tax_code)])
            for ft in foreign_taxes:
                if int(ft.income)/100 <= emp.contract_id.wage and emp.contract_id.wage < int(ft.income)/100+int(ft.next_tax_rule)/100:
                    emp.foreign_tax_id = ft

    foreign_tax_id = fields.One2many('payroll.foreign.tax', 'employee_id', string="Foreign Tax", compute='_compute_foreign_tax_id', store=True)
