from flectra.tools import convert_file
from flectra import models, api, fields, _
import glob
import csv
import time
import os


def import_foreign_tax_data(cr, registry):
    env = api.Environment(cr, 1, {})
    # temp = env['payroll.foreign.tax'].search([])

    path = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'data', 'foreign_tax')
    txt_files = glob.glob(f"{path}/*/*.txt", recursive=True)
    result = []
    sql = "INSERT INTO payroll_foreign_tax (row_type,transaction_type,canton,qst_code,extra,income,next_tax_rule,gender,children_amount,tax_in_chf,tax_in_percentage,code_state) VALUES "
    for file in txt_files:
        txf = open(file, 'r')
        txt_lines = txf.readlines()
        txf.close()
        if len(txt_lines):
            print(f"Process file: {file}")
            temp = process_textlines(txt_lines)
            # result.extend(temp)
            cr.execute(sql + temp.rstrip(',')+';')
    # env['payroll.foreign.tax'].create(result)

    print("*"*20, sql)


def process_textlines(txt_lines: list):
    # csv_rows = []
    csv_rows = ''
    for line in txt_lines:
        # csv_rows.append(format_code(line))
        csv_rows = csv_rows + str(format_code(line))+','
    return csv_rows


def process_textlines_bk(txt_lines: list):
    csv_rows = []
    for line in txt_lines:
        csv_rows.append(format_code(line))

    if len(csv_rows):
        keys = csv_rows[0].keys()
        with open('foreign_tax.csv', 'w', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(csv_rows)


def format_code(opt: str):
    return (opt[0:2].strip() if len(opt) >= 2 else '',
            opt[2:4].strip() if len(opt) >= 4 else '',
            opt[4:6].strip() if len(opt) >= 6 else '',
            opt[6:16].strip() if len(opt) >= 16 else '',
            opt[16:24].strip() if len(opt) >= 24 else '',
            opt[24:33].strip() if len(opt) >= 33 else '',
            opt[33:42].strip() if len(opt) >= 42 else '',
            opt[42:43].strip() if len(opt) >= 43 else '',
            opt[43:45].strip() if len(opt) >= 45 else '',
            opt[45:54].strip() if len(opt) >= 54 else '',
            opt[54:59].strip() if len(opt) >= 59 else '',
            opt[59:62].strip() if len(opt) >= 62 else ''
            )
