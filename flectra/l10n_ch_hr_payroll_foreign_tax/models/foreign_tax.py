
from flectra import models, api, fields, _
from flectra.exceptions import UserError, ValidationError, MissingError
import flectra.tools as tools
import glob
import os

class ForeignTax(models.Model):
    _name = "payroll.foreign.tax"

    def set_foreign_tax(self):
        self.import_foreign_tax()

    def import_foreign_tax(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)))
        path = os.path.join(os.path.dirname(path),'data','foreign_tax')
        txt_files = glob.glob(f"{path}/*/*.txt",recursive=True)
        print("*"*50, txt_files)
        result = []
        for file in txt_files:
            txf = open(file,'r')
            txt_lines = txf.readlines()
            txf.close()
            if len(txt_lines):
                print(f"Process file: {file}")
                temp = self.process_textlines(txt_lines=txt_lines)
                result.extend(temp)
        self.env['payroll.foreign.tax'].create(result)
        print("result",len(result))
       

    def process_textlines(self,txt_lines:list):
        csv_rows = []
        for line in txt_lines:
            csv_rows.append(self.format_code(opt=line))
        print("*"*50 ,len(csv_rows))
        return csv_rows
        # self.create(csv_rows)
        
    def format_code(self, opt:str):
        return {
            'row_type': opt[0:2] if len(opt) >= 2 else None,
            'transaction_type':opt[2:4] if len(opt) >= 4 else None,
            'canton':opt[4:6] if len(opt) >= 6 else None,
            'qst_code':opt[6:16] if len(opt) >= 16 else None,
            'extra':opt[16:24] if len(opt) >= 24 else None,
            'income':opt[24:33] if len(opt) >= 33 else None,
            'next_tax_rule':opt[33:42] if len(opt) >= 42 else None,
            'gender':opt[42:43] if len(opt) >= 43 else None,
            'children_amount':opt[43:45] if len(opt) >= 45 else None,
            'tax_in_chf':opt[45:54] if len(opt) >= 54 else None,
            'tax_in_percentage':opt[54:59] if len(opt) >= 59 else None,
            'code_state':opt[59:62] if len(opt) >= 62 else None
        }


    row_type = fields.Char(string='Tax Rule')
    transaction_type = fields.Char()
    canton = fields.Char()
    qst_code = fields.Char(string='Tax Code')
    extra = fields.Char()
    income = fields.Char()
    next_tax_rule = fields.Char()
    gender = fields.Char()
    children_amount = fields.Char()
    tax_in_chf = fields.Char()
    tax_in_percentage = fields.Char()
    code_state = fields.Char()
    
    employee_id  = fields.Many2one('hr.employee')

# class ForeignTaxLogs(models.Model):
#     _name = "payroll.foreign.tax.logs"

#     tax_filename =  fields.Char()
#     status = fields.Boolean()
    