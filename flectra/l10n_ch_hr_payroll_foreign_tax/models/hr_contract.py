# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import api, fields, models, _
from flectra.exceptions import ValidationError

class Contract(models.Model):
    _inherit = 'hr.contract'

    @api.depends('tax_code', 'wage')
    def call_to_foreign_tax(self):
        for contract in self:
            contract.employee_id._compute_foreign_tax_id()

    tax_code = fields.Char(string='Tax Code')

    @api.model
    def create(self, vals):
        values = super(Contract, self).create(vals)
        self.call_to_foreign_tax()
        return values

    def write(self, vals):
        values = super(Contract, self).write(vals)
        self.call_to_foreign_tax()
        return values