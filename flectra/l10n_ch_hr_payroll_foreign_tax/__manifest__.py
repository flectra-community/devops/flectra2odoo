{
    "name": "Payroll Foreign Tax",
    "version": "2.0",
    'description': """
Payroll Foreign Tax Responsible for additional tax on payslip
""",
    'category': 'Human Resources/Employees',
    'installable': True,
    'application': True,
    'auto_install': False,
    "license": "",
    "author": "2BIT GmbH",
    "website": "https://www.2bit.ch",
    'price': 0.00,
    'currency': 'CHF',
    "summary": "Additional tax on payslip",
    "depends": ['l10n_ch_hr_payroll_base'],
    "post_init_hook": "import_tax",
    # "pre_init_hook": "create_tax_csv",
    # "post_load": "create_tax_csv",
    'data': [
        'security/ir.model.access.csv',
        'data/hr.salary.rule.xml',
        'views/foreign_tax_views.xml',
        'views/hr_contract_views.xml'
        # 'data/automatic_import_foreign_tax.xml'
        # 'views/employee_hr_setting.xml'
        
    ],
    

}