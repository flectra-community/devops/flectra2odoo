# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from datetime import date

from flectra import api, fields, models, _
from flectra.exceptions import UserError, ValidationError, AccessError

class HrEmployeeChildren(models.Model):
    _name = "hr.employee.children"

    @api.depends('hr_employee_id', 'hr_employee_id.address_id')
    def _compute_child_allowance_rule(self):
        for child in self:
            child.child_allowance_rule_id = self.child_allowance_rule_id.search([('region', '=', child.hr_employee_id.address_id.state_id.code)]).id

    @api.depends('date_of_birth')
    def _compute_child_age(self):
        for child in self:
            if child.date_of_birth:
                child.age = date.today().year - child.date_of_birth.year

    """@api.depends('age', 'further_education', 'child_allowance_rule_id')
    def _compute_child_allowance(self):
        for child in self:
            child.age = date.today().year - child.date_of_birth.year """

    name = fields.Char(string='Child Name', required=True)
    date_of_birth = fields.Date(string='Date of Birth', required=True)
    further_education = fields.Boolean(string='Is in further education?')
    age = fields.Integer(string='Child Age', compute='_compute_child_age', store=True)

    child_birth = fields.Boolean(string='Child Birth')
    child_adoption = fields.Boolean(string='Child Adoption')

    hr_employee_id = fields.Many2one('hr.employee', string='Employee')
    child_allowance_rule_id = fields.Many2one('child.allowance.rule', string='Child Allowance Rule', compute='_compute_child_allowance_rule', store=True)
