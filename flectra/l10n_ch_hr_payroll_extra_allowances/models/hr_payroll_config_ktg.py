# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import api, fields, models, _
from flectra.exceptions import ValidationError, AccessError

class ChildAllowanceRule(models.Model):
    _name = "hr.payroll.config.ktg"

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', required=True)
    male_percentage = fields.Integer(string='Male Percentage')
    female_percentage = fields.Integer(string='Female Percentage')
    