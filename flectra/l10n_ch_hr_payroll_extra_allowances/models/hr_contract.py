# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import api, fields, models, _
from flectra.exceptions import ValidationError

class Contract(models.Model):
    _inherit = 'hr.contract'

    ktg_solutions = fields.Many2one('hr.payroll.config.ktg', string='KTG Solutions')
