# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from . import hr_employee
from . import hr_employee_children
from . import child_allowance_rule
from . import hr_payroll_config_ktg
from . import hr_payroll_config
from . import hr_contract