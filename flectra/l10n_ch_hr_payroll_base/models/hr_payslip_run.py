import logging

from datetime import timedelta

from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class HrPayslipRun(models.Model):
    # Private attributes
    _inherit = 'hr.payslip.run'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods
    def generate_payslip_for_active_employee(self):
        self.ensure_one()
        HrPayslip = self.env['hr.payslip']
        HrEmployee = self.env['hr.employee']
        HrContract = self.env['hr.contract']

        wage_type = self.env.context.get('wage_type', False)
        if wage_type == 'month' or not wage_type:
            month_contracts = HrContract.search([
                ('wage_type', '=', 'month'),
                ('date_start', '<=', self.date_start),
                '|',
                ('date_end', '=', False),
                ('date_end', '>=', self.date_start),
            ])
            if month_contracts:
                HrEmployee = month_contracts.mapped('employee_id')

        if wage_type == 'hour' or not wage_type:
            hour_contracts = HrContract.search([
                ('wage_type', '=', 'hour'),
                ('date_start', '<=', self.date_start),
                '|',
                ('date_end', '=', False),
                ('date_end', '>=', self.date_start),
            ])
            if hour_contracts:
                for emp in hour_contracts.mapped('employee_id'):
                    if self._check_for_work_time(emp):
                        HrEmployee += emp

        for emp in HrEmployee:
            slip_data = self.env['hr.payslip'].onchange_employee_id(
                    self.date_start, self.date_end,
                    emp.id, contract_id=emp.contract_id
            )

            res = {
                'employee_id': emp.id,
                'name': slip_data['value'].get('name'),
                'struct_id': slip_data['value'].get('struct_id'),
                'contract_id': slip_data['value'].get('contract_id'),
                'payslip_run_id': self.id,
                'input_line_ids': [(0, 0, x) for x in slip_data['value'].get('input_line_ids')],
                'worked_days_line_ids': [(0, 0, x) for x in slip_data['value'].get('worked_days_line_ids')],
                'date_from': self.date_start,
                'date_to': self.date_end,
                'company_id': emp.company_id.id,
            }
            HrPayslip += HrPayslip.create(res)

        HrPayslip.compute_sheet()

    def _check_for_work_time(self, employee):
        date_end = fields.Date.from_string(self.date_end) + timedelta(days=1)
        HrAttendance = self.env['hr.attendance']
        return HrAttendance.search_count([
            ('employee_id', '=', employee.id),
            ('check_in', '>=', self.date_start),
            ('check_in', '<', fields.Date.to_string(date_end)),
        ])

    def confirm_all_payslips(self):
        self.ensure_one()
        self.slip_ids.filtered(lambda f: f.state == 'draft').action_payslip_done()
        self.close_payslip_run()

    def recompute_all_payslips(self):
        self.ensure_one()
        self.slip_ids.filtered(lambda f: f.state == 'draft').compute_sheet()

