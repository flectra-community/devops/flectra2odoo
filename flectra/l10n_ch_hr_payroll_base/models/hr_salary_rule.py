# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from flectra import fields, models, api
from flectra.tools.safe_eval import safe_eval


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    name = fields.Char(translate=True)
    note = fields.Char(translate=True)

    percentage = fields.Float(
        compute="_compute_percentage_from_company",
        required=False)
    amount_base = fields.Float(
        required=False)
    
    report_is_total_rule = fields.Boolean(
        string='Is total rule',
    )
    report_display_details_columns = fields.Boolean(
        string='Display details columns',
        default=True,
    )
    report_is_net_amount = fields.Boolean(
        string='Is net amount',
    )

    def _compute_percentage_from_company(self):
        list_fields_per = {
            'fadmin_per': ['FADMIN'],
            'avs_per': ['AVS_C', 'AVS_E'],
            'laa_per': ['LAA_C', 'LAA_E'],
            'lca_per': ['LCA_C', 'LCA_E'],
            'ac_per_off_limit': ['AC_C_SOL', 'AC_E_SOL'],
            'ac_per_in_limit': ['AC_C', 'AC_E'],
            'psd_per_male': ['PSD_C_MALE', 'PSD_E_MALE'],
            'psd_per_female': ['PSD_C_FEMALE', 'PSD_E_FEMALE'],
        }

        for rule in self:
            for rule_from, rules_to in list(list_fields_per.items()):
                for rule_to in rules_to:
                    data_id = self.env['ir.model.data'].search([
                        ('module', '=', 'l10n_ch_hr_payroll_base'),
                        ('name', '=', rule_to)
                    ])

                    if len(data_id):
                        rule_to_modify = rule.env['hr.salary.rule'].search([
                            ('id', '=', data_id.res_id)
                        ])

                        if rule_to_modify.id == rule.id:
                            rule.percentage = \
                                getattr(rule.company_id, rule_from)
            rule.percentage = rule.percentage if rule.percentage else 0.0

    def _compute_rule(self, localdict):

        # FIX: swiss rule codes are numeric but python does not allow variable names starting
        # with a number so we add a var_ as prefix for these variables
        for k, v in localdict.copy().items():
            if k[:1] in '0123456789':
                new_key = 'var_' + k
                localdict[new_key] = v

        res = super()._compute_rule(localdict)

        for rule in self:
            if rule.amount_percentage_base:
                rule.amount_base = \
                    float(safe_eval(rule.amount_percentage_base, localdict))
            if rule.id == self.env.ref("l10n_ch_hr_payroll_base.LPP_C").id or \
                    rule.id == self.env.ref("l10n_ch_hr_payroll_base.LPP_E").id:
                rule.percentage = \
                    -float(safe_eval("contract.lpp_rate or 100", localdict))

            if rule.id == self.env.ref("l10n_ch_hr_payroll_base.IMP_SRC").id:
                rule.percentage = \
                    -float(safe_eval("contract.imp_src or 100", localdict))
        return res
