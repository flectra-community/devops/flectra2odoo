# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging

from flectra import fields, models, api
import flectra.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class HrContract(models.Model):
    _inherit = 'hr.contract'

    lpp_rate = fields.Float(
        string='OBP Rate (%)',
        digits=dp.get_precision('Payroll Rate'))
    lpp_amount = fields.Float(
        string='OBP Amount',
        digits=dp.get_precision('Account'))
    lpp_contract_id = fields.Many2one(
        string='OBP Contract',
        comodel_name='lpp.contract',
        inverse_name='contract_id')

    imp_src = fields.Float(
        string='Source Tax (%)',
        digits=dp.get_precision('Payroll Rate'))

    wage_type = fields.Selection(
        string="Wage Type",
        selection=[('month', "Monthly"), ('hour', "Hourly")],
        default='month')
    wage_fulltime = fields.Float(
        string='Full-time Wage',
        digits=dp.get_precision('Account'),
        default=0)
    occupation_rate = fields.Float(
        string='Occupation Rate (%)',
        digits=dp.get_precision('Account'),
        default=100.0)
    
    expense_lump_sum_car = fields.Float(
            string='Lump Sum Car',
            digits=dp.get_precision('Account'),
            default=0)
    expense_lump_sum_representation = fields.Float(
            string='Lump Sum Representation',
            digits=dp.get_precision('Account'),
            default=0)
    expense_lump_sum_other = fields.Float(
            string='Lump Sum Other',
            digits=dp.get_precision('Account'),
            default=0,
    )
    expense_lump_sum_other_text = fields.Text(
            string='Lump Sum Other Text',
    )
    no_charge_declaration = fields.Boolean(
            string='No Charge Declaration',
            help='Check this field, if there are no effective charges to be declared separately',
    )
    payroll_remark = fields.Char(
            string='Payroll Remark',
            translate=True,
            help='Additional remark to print on payroll. It will be appended to remark field',
    )
    vehicle_purchase_price = fields.Float(
            string='Vehicle Purchase Price',
            help='For private use, 0.8% of the purchase price (but at least 150.00) per month is deducted from the '
                 'employee\'s salary.',
            digits=dp.get_precision('Account'),
            default=0,
    )

    @api.onchange('occupation_rate', 'wage_fulltime')
    def _onchange_wage_rate_fulltime(self):
        for contract in self:
            contract.wage = \
                contract.wage_fulltime * (contract.occupation_rate / 100)
