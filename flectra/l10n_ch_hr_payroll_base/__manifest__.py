# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Switzerland - Payroll Base',
    'summary': 'Switzerland Payroll Rules',
    'category': 'Localization',
    'author': "Open Net Sàrl,Odoo Community Association (OCA), 2BIT GmbH",
    'depends': [
        "hr",
        "hr_payroll",
        'hr_payroll_account',
        "hr_expense",
        'hr_contract',
        'hr_attendance'
    ],
    'version': '2.0.1.0.0',
    'auto_install': False,
    'demo': [],
    'website': 'https://gitlab.com/flectra-community/l10n-switzerland',
    'license': 'AGPL-3',
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',

        'data/hr_payroll_category.xml',
        'data/hr.salary.rule.category.xml',
        'data/hr.salary.rule.xml',

        'views/hr_salary_rule_view.xml',
        'views/hr_contract_view.xml',
        'views/hr_employee_view.xml',
        'views/hr_payroll_view.xml',
        'views/hr_payroll_config_view.xml',
        'views/hr_payslip_view.xml',
        'views/hr_payslip_line_view.xml',
        'views/lpp_contract_view.xml',
        'views/hr_payslip_run_views.xml',
        "views/hr_payslip_yearly_report.xml",
        "views/menu.xml",

        "reports/report_payslip.xml",
        'reports/payroll_structure_report_view.xml',
    ],
    'demo': [
        
    ],
    'installable': True
}
