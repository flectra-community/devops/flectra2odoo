import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class HrPayrollConfig(models.TransientModel):
    # Private attributes
    _inherit = 'hr.payroll.config'

    # Default methods

    # Fields declaration
    address_alignment = fields.Selection(
        string='Address Alignment',
        related='company_id.address_alignment',
        default=lambda self: self.env.user.company_id.address_alignment,
    )

    # Compute and search methods, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
