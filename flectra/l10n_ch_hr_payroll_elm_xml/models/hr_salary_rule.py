from flectra import models, fields, api, _


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    salary_statement_group = fields.Selection(
        string='Salary Statement Group',
        selection=[
            ('income', 'Income'),
            ('gross', 'Gross Income'),
            ('net', 'Net Income'),
            ('fringe_bene_car', 'Fringe Benefit Car'),
            ('other_bene', 'Other Benefits'),
            ('oai', 'OAI'),
            ('obp', 'OBP'),
            ('education', 'Education'),
            ('lump_sum_rep', 'Lump Sum Representation'),
            ('lump_sum_car', 'Lump Sum Car'),
            ('lump_sum_other', 'Lump Sum Others'),
            ('effective', 'Effective Others'),
            ('fam_allowance', 'Family Allowance'),
            ('source_deduction', 'Deduction at Source'),
            ('remark', 'Remark'),
        ],
    )
