import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    # Private attributes
    _inherit = 'res.company'

    # Default methods

    # Fields declaration
    address_alignment = fields.Selection(
        string='Address Alignment',
        selection=[
            ('RIGHT', 'RIGHT'),
            ('LEFT', 'LEFT'),
        ],
        default='LEFT',
        help='Define on which side the employee\'s address shall be displayed on the PDF salary declaration',
    )

    # Compute and search methods, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
