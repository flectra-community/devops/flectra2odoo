Payroll ELM XML Export
======================

Export an importable xml file with payroll data. This data can be used to generate salary declaration pdf documents with
online tools such as https://www.elohnausweis-ssk.ch/

Usage
=====

To use this module, you need to:

* Expense regulation: If there is an expense regulation approved by a canton/state, check the corresponding box to print the approval statement with date on payment slips.

Known issues
============


Credits
=======

Authors
-------

* Jamotion GmbH

Contributors
------------

* Renzo Meister <rm@jamotion.ch>
* Sébastien Pulver <sp@jamotion.ch>
